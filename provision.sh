#!/usr/bin/env bash

# Vagrant plugin runs bvefore provisioning to 
# Install and build Guest Additions, will see gcc,kernel-devel.. insatlled

# Add the Extra Packages for Enterprise Linux (EPEL)
sudo yum install -y epel-release

# Update the OS to latest packages 
sudo yum -y update

# Install the common base packages 
sudo yum install -y vim-enhanced

# Install git from EPEL 
if ! hash git 2>/dev/null; then
	sudo yum install -y git 
fi

# Create the user 
user=deployer
[ -d /home/$user ] || sudo /usr/sbin/useradd -m $user

# Give $user SUDO privilages
has_sudo=$( sudo -n -l -U "$user" 2>&1 | egrep -c -i "\(ALL\)\sNOPASSWD:\sALL" )
[ "$has_sudo" -ne "0" ]  || echo "$user ALL=(ALL) NOPASSWD: ALL" | sudo tee -a /etc/sudoers	

# Setup $user ssh 
github_user='farooqsadiq'
bitbucket_key 

setup_ssh=$(cat <<-EOF
	cd /home/$user 
	. .bash_profile

	if [ ! -d .ssh ]; then
		mkdir .ssh
		chmod 700 .ssh
		# Centos 5 requires -t, does not support -H
		ssh-keyscan -T10 -t rsa bitbucket.org >> ~/.ssh/known_hosts
		ssh-keyscan -T10 -t rsa 104.192.143.1 >> ~/.ssh/known_hosts
		ssh-keyscan -T10 -t rsa github.org >> ~/.ssh/known_hosts
		wget https://github.com/$github_user.keys -O .ssh/authorized_keys
		chmod 600 .ssh/authorized_keys

		git config --global user.email deployer.library@jhu.edu
		git config --global user.name "Deployer JHU Library"
	fi
EOF
	)
sudo -u $user -H bash -c "$setup_ssh" 

# Install prerequisits to build rubies, used by rvm 
sudo yum install -y gcc-c++ patch readline readline-devel zlib zlib-devel 
sudo yum install -y libyaml-devel libffi-devel openssl-devel make 
sudo yum install -y bzip2 autoconf automake libtool bison iconv-devel sqlite-devel

# Install rvm as user TODO IS NOT idempotent 	
install_rvm=$(cat <<-EOF
	cd /home/$user 
	. .bash_profile

	if ! hash rvm 2>/dev/null; then

		#gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
		curl -sSL https://rvm.io/mpapis.asc | gpg --import -
 		curl -L get.rvm.io | bash -s stable
		. ~/.bash_profile
		rvm install 2.2.2
 		rvm use 2.2.2 --default
 	fi
EOF
	)
sudo -u $user -H bash -c "$install_rvm" 

# Install Apache
sudo yum install -y httpd 


# Install Passenger
if [ ! "$(sudo /usr/sbin/httpd -M  2>&1 | grep passenger_module)" ]; then

  # Install Prerequisits to build passenger module
	sudo yum install -y curl-devel httpd-devel apr-devel apr-util-devel

	# Install passenger gem 
	install_passenger=$(cat <<-EOF
	cd /home/$user 
	. .bash_profile
	gem install passenger
	passenger-install-apache2-module --auto
	passenger-install-apache2-module --snippet > passenger_apache
	passenger-install-apache2-module --snippet | sudo tee -a /etc/httpd/conf/httpd.conf
EOF
	)
	sudo -u $user -H bash -c "$install_passenger" 

fi


# Create App Base Directory
apps_base="/opt/apps"
if [ ! -d "$apps_base" ]; then
	sudo mkdir -p $apps_base
	sudo chown $user:$user $apps_base
  umask 0002
	sudo chmod g+s $apps_base
fi


# Deploy Test App
app_name="testapp"
app_dir="$apps_base/$app_name"

if [ ! -d "$appdir" ]; then

	app_conf=$(cat <<-EOF
	  Alias /$app_name $app_dir/current/public
	  <Location /$app_name>
	    PassengerBaseURI /$app_name
	    PassengerAppRoot $app_dir/current
	  </Location>
	  <Directory $app_dir/current/public>
	    Allow from all
	    Options -MultiViews
	  </Directory>
EOF
	)

	deploy_testapp=$(cat <<-EOF
	  cd /home/$user
	  . .bash_profile
	  echo "$app_conf" | sudo tee "/etc/httpd/conf.d/$app_name.conf"
	  sudo chown -R  $user:$user $app_dir
		mkdir -p $app_dir/shared/config $app_dir/releases
		rsync -rv --progress /vagrant/secrets/secrets.yml $app_dir/shared/config/
	  git clone git@bitbucket.org:farooqsadiq/$app_name.git $app_dir/releases/test
		ln -s $app_dir/releases/test $app_dir/current
		ln -s $app_dir/shared/config/secrets.yml $app_dir/releases/test/config/secrets.yml
	  gem install bundle
	  cd $app_dir/releases/test
	  bundle install
EOF
	)
	sudo -u $user -H bash -c "$deploy_testapp" 
	sudo /sbin/service httpd restart
fi

