# Sinatra App to demonstrate Capistirano deploy to Vagrant

This is a simple Sinatra application which displpays hello world.
Used as a proof-of-concept and skills transfer tool. 
Goals
- Deploy to local vagrant, pull from public repo
- Deploy to local vagrant, pull from private repo

Kudos to Igor Šarčević for the excellent tutorial post: 
[How to Deploy Sinatra Applications with Capistrano](https://semaphoreci.com/community/tutorials/how-to-deploy-sinatra-applications-with-capistrano)


## Deploying to staging 

To check that ssh-forward is enabled you can
```bash
ssh -p2222  deployer@localhost 'echo "$SSH_AUTH_SOCK";ssh -T git@bitbucket.org'
```

Check  git access
```bash
ssh -p2222  deployer@localhost 'git ls-remote git@bitbucket.org:farooqsadiq/testapp.git'
```


References 
* [Capistrano Authentication Setup](http://capistranorb.com/documentation/getting-started/authentication-and-authorisation/#
* [Using SSH agent forwarding](https://developer.github.com/guides/using-ssh-agent-forwarding/)

